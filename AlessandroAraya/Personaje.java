public class Personaje
 {
	private String nombre;
	private int vida;
	private int poder;
	private boolean esPrincipal;

	public Personaje()
	{
		
	nombre = "Computer";
	vida= 3;
	poder= 100;
	esPrincipal=false;
	
	}
	
	public Personaje(String nombre)
	{
		
	setNombre(nombre);
	vida = 3;
	poder = 100;
	esPrincipal = true;
	
	}

	public Personaje(String nombre, int vida, int poder)
	{
		
	 setNombre(nombre);
	 setVida(vida);
	 setPoder(poder);
	 esPrincipal = true;
	 
	}
	
	
	public void setNombre(String nombre)
    {
		
		this.nombre=nombre;
	
	}
	
	public String getNombre()
	{

		return nombre;
	
	}
	
	public void setVida(int vida)
	
	{
		this.vida=vida;
	}
	
	public int getVida()
	
	{
	    return vida;
	}
	 
	public void setPoder(int poder)
	
	{
		this.poder = poder;
	}
	
	public int getPoder()
	
	{
		return poder;
	}
	 
	public void setEsPrincipal(boolean esPrincipal)
	
	{
	
	 this.esPrincipal=esPrincipal;
	 
	}

	public boolean getEsPrincipal()
	 
	{
	 return esPrincipal;
	}

	public String toString()
	 
	{
		 
	 String personaje;
	 
	 if (esPrincipal)
	 {
		 
		personaje="principal";
	 
	 }
	 else
	 {
		 
		personaje="enemigo";
	 
	 }
	 
	 return getNombre() + " tiene " + getVida() + " vidas, un poder de "
	 + getPoder() +" y es un personaje "+ personaje;
	 
	 }
	 
}
