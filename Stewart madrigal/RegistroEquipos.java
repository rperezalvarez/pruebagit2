
public class RegistroEquipos
{
	private Equipo equipo1;
	private Equipo equipo2;
	private Equipo equipo3;
	private int longitud;
	
	
	public RegistroEquipos()
	{
		longitud = 0;
	}
	
	public void setEquipo(Equipo equipo)
	{
		if(longitud==0)
		{
			this.equipo1 = equipo;
			longitud++;
		}
		else
		{
			if(longitud==1)
			{
				equipo2 = equipo;
				longitud++;
			}
			else
			{
				if(longitud==2)
				{
					equipo3 = equipo;
					longitud++;
				}
			}
		}	
	}
	
	
	public Equipo getEquipo(int numeroEquipo)
	{
		if(numeroEquipo==0)
		return equipo1;
		else
			if(numeroEquipo==1)
			return equipo2;
			else
				if(numeroEquipo==2)
				return equipo3;
				else
					return null;
	}
	
	public void setLongitud(int longitud)
	{
		this.longitud = longitud;
	}
	
	public int getLongitud()
	{
		return longitud;
	}
	
	public String toString()
	{
		String informacion="";
		
		if(equipo1!= null)
			informacion += equipo1.toString();
			
		if(equipo2!= null)
			informacion += equipo2.toString();
			
		if(equipo3!= null)
			informacion += equipo3.toString();
		
		return informacion;
	}
	
	
}//Fin clase Registro de equipos
