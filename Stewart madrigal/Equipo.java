import javax.swing.JOptionPane;

public class Equipo 
{
	private String placa;
	private String marca;
	private String modelo; 
	private String especificaciones;
	
	//METODO CONSTRUCTOR
	public Equipo()
	{
		setPlaca("");
		setMarca("");
		setModelo("");
		setEspecificaciones("");
	}
	
	public Equipo (String placa, String marca, String modelo, String especificaciones)
	{
		setPlaca(placa);
		setMarca(marca);
		setModelo(modelo);
		setEspecificaciones(especificaciones);
	}
	
	
	//METODOS SET
	public void setPlaca(String placa)
	{
		this.placa = placa;
	}
	
	public void setMarca(String marca)
	{
		this.marca = marca;
	}
	
	public void setModelo(String modelo)
	{
		this.modelo = modelo;
	}
	
	public void setEspecificaciones(String especificaciones)
	{
		this.especificaciones = especificaciones;
	}
	
	
	//METODOS GET
	public String getPlaca()
	{
		return placa;
	}
	
	public String getMarca()
	{
		return marca;
	}
	
	public String getModelo()
	{
		return modelo;
	}
	
	public String getEspecificaciones()
	{
		return especificaciones;
	}
	
	public String toString()
	{
		return "Placa: "+getPlaca()+"\nMarca: "+getMarca()+"\nModelo: "+getModelo()+"\nEspecificaciones: "+getEspecificaciones();
	}
	
	

}
