

public class Equipo
{	
		
	private String placa;
	private String marca;
	private String especificaciones;
	private String modelo; 
	
	//constructores
	
	public Equipo()
	{
	
		placa = "";
		marca = "";
		especificaciones = "";
		modelo = ""; 
		
	} 
	
	public Equipo(String numPlaca, String marca, String especificaciones, String modelo)
	{
		setPlaca(numPlaca);
		setMarca(marca);
		setEspecificaciones(especificaciones);
		setModelo(modelo);
	}

	//set, get y acciones

	public void setPlaca(String numPlaca)
	{
		placa = numPlaca; 
	}
	
	public String getPlaca()
	{
		return placa;
	}

	public void setMarca(String marca)
	{
		this.marca = marca; 
	}
	
	public String getMarca()
	{
		return marca;
	}
	
	public void setEspecificaciones(String especificaciones)
	{
		this.especificaciones = especificaciones;
	}	
		
	public String getEspecificaciones()
	{
		return especificaciones;
	}
		
	
	public void setModelo(String modelo)
	{
		this.modelo = modelo;
	}
	
	public String getModelo()
	{
		return modelo;
	}
	
	public String toString()
	{
		return "La placa del equipo es: " + getPlaca() +"\nLa marca del equipo es: " + getMarca() + "\nLas especificaciones del equipo son: " + getEspecificaciones()+ "\nEl modelo del equipo es: " + getModelo();
	}
	
	
}//fin de la clase



