public class RegistroEquipo
{
	Equipo equipo1;
	Equipo equipo2;
	Equipo equipo3;
	int longitud;
	
	public RegistroEquipo()
	{
		longitud=0;
	}
	
	public void setEquipo(Equipo equipo)
	{
		if (longitud == 0){
		    equipo1 = equipo;
		    longitud++;
		}
		else 
		   if (longitud == 1){
		     equipo2 = equipo;
		     longitud++;
		 }
		else
		   if (longitud == 2){
		      equipo3 = equipo;
		      longitud++;
		  }
	}
	
	public Equipo getEquipo(int numEquipo)
	{
		if(numEquipo == 0)
		   return equipo1;
		   
		   else
		   
		    if(numEquipo == 1)
		     return equipo2;
		     
		      else
		      
		       if(numEquipo == 2)
		        return equipo3;
		        
		else
		         
		return null;
	}
	
	public void setLongitud(int longitud)
	{
		this.longitud=longitud;
	}
	
	public int getLongitud()
	{
		return longitud;
	}
	
	public String toString()
	{
		
		return equipo1.toString() + " " + equipo2.toString() + " " + equipo3.toString();  
		
	}
	
	
}
